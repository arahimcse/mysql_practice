link@ https://dev.mysql.com/doc/refman/8.0/en/select.html

The LIMIT clause is used to specify the number of records to return.

The LIMIT clause is useful on large tables with thousands of records. Returning a large number of records can impact performance.

**** has two parameters, these are offset and number of records

example limit 2, 5 means 
--- skip the first 3 elements [table store the record as a array. we know array start from index 0, so 0 to 2 = 3 (0, 1, 2)]



****original values into the table
select * from employee;
+----+------+------+-------------+--------+
| id | name | age  | performance | salary |
+----+------+------+-------------+--------+
|  1 | a    |   32 |           1 |  50000 |
|  6 | b    |   30 |           3 |  66950 |
|  7 | c    |   25 |           4 |  78750 |
|  8 | d    |   29 |           5 | 135000 |
|  9 | e    |   36 |           3 |  87550 |
| 10 | f    |   32 |        NULL |  50750 |
+----+------+------+-------------+--------+
6 rows in set (0.00 sec)

***limit without offset***
select * from employee limit 3;
+----+------+------+-------------+--------+
| id | name | age  | performance | salary |
+----+------+------+-------------+--------+
|  1 | a    |   32 |           1 |  50000 |
|  6 | b    |   30 |           3 |  66950 |
|  7 | c    |   25 |           4 |  78750 |
+----+------+------+-------------+--------+
3 rows in set (0.00 sec)

show the first three records from the table

**with offset**
mysql> select * from employee limit 1, 2;
+----+------+------+-------------+--------+
| id | name | age  | performance | salary |
+----+------+------+-------------+--------+
|  6 | b    |   30 |           3 |  66950 |
|  7 | c    |   25 |           4 |  78750 |
+----+------+------+-------------+--------+
2 rows in set (0.00 sec)

limit 1, 2 means skip first 1 record then show the next 2 records retrive[1, 2 row(row start from 0)]. 

**** using prepare statement ****
mysql> set @skip=1;
Query OK, 0 rows affected (0.01 sec)

mysql> set @numrows = 2;
Query OK, 0 rows affected (0.00 sec)

mysql> prepare stmt from 'select * from employee limit ?, ?';
Query OK, 0 rows affected (0.02 sec)
Statement prepared

mysql> execute stmt using @skip, @numrows;
+----+------+------+-------------+--------+
| id | name | age  | performance | salary |
+----+------+------+-------------+--------+
|  6 | b    |   30 |           3 |  66950 |
|  7 | c    |   25 |           4 |  78750 |
+----+------+------+-------------+--------+
2 rows in set (0.01 sec)

Also, we can attached the where condition, like
