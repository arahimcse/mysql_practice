The MySQL EXISTS Operator
The EXISTS operator is used to test for the existence of any record in a subquery.

The EXISTS operator returns TRUE if the subquery returns one or more records.

***
mysql> select * from merits;
+----+------------+
| id | percentage |
+----+------------+
|  1 |          0 |
|  2 |       0.01 |
|  3 |       0.03 |
|  4 |       0.05 |
|  5 |       0.08 |
|  6 |       0.09 |
+----+------------+
6 rows in set (0.00 sec)



****
mysql> select * from employee where exists (select id from merits);
+----+-------------------+------+-------------+--------+
| id | name              | age  | performance | salary |
+----+-------------------+------+-------------+--------+
|  1 | a                 |   32 |           1 |  50000 |
|  6 | b                 |   30 |           3 |  66950 |
|  7 | c                 |   25 |           4 |  78750 |
|  8 | d                 |   29 |           5 | 135000 |
|  9 | e                 |   36 |           3 |  87550 |
| 10 | f                 |   32 |        NULL |  50750 |
| 11 | abdur rahim       |   30 |           2 |  25000 |
| 12 | sumaia aktar      |   30 |           1 |  18881 |
| 13 | md. razwon ahmmed |    4 |           4 |      0 |
+----+-------------------+------+-------------+--------+
9 rows in set (0.00 sec)

subquery return true so, where cluse true. and the query retrive record from employee table


***
mysql> select * from employee where exists (select id from merits where id=2);
+----+-------------------+------+-------------+--------+
| id | name              | age  | performance | salary |
+----+-------------------+------+-------------+--------+
|  1 | a                 |   32 |           1 |  50000 |
|  6 | b                 |   30 |           3 |  66950 |
|  7 | c                 |   25 |           4 |  78750 |
|  8 | d                 |   29 |           5 | 135000 |
|  9 | e                 |   36 |           3 |  87550 |
| 10 | f                 |   32 |        NULL |  50750 |
| 11 | abdur rahim       |   30 |           2 |  25000 |
| 12 | sumaia aktar      |   30 |           1 |  18881 |
| 13 | md. razwon ahmmed |    4 |           4 |      0 |
+----+-------------------+------+-------------+--------+
9 rows in set (0.00 sec)

subquery also true. so, same results provides

***
mysql> select * from employee where exists (select id from merits where id=10);
Empty set (0.00 sec)

subquery is false. so where cluse is not true and the result is empty

 