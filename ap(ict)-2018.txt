Two table is given employee and department
1. Find employee info from employee table who had joined between the date 2013-01-21 to 2018-01-31
2. Find employee id, name, avg salary who are in sales department show them in ascending oerder.

mysql> select * from employee;
+----+-------------------+------+-------------+--------+--------+------------+
| id | name              | age  | performance | salary | dep_id | join_data  |
+----+-------------------+------+-------------+--------+--------+------------+
|  1 | a                 |   32 |           1 |  50000 |      1 | 2013-01-01 |
|  6 | b                 |   30 |           3 |  66950 |      1 | 2019-02-24 |
|  7 | c                 |   25 |           4 |  78750 |      3 | 2015-03-04 |
|  8 | d                 |   29 |           5 | 135000 |      5 | 2016-05-14 |
|  9 | e                 |   36 |           3 |  87550 |      1 | 2018-04-20 |
| 10 | f                 |   32 |        NULL |  50750 |   NULL | 2017-09-01 |
| 11 | abdur rahim       |   30 |           2 |  25000 |   NULL | NULL       |
| 12 | sumaia aktar      |   30 |           1 |  18881 |   NULL | NULL       |
| 13 | md. razwon ahmmed |    4 |           4 |      0 |   NULL | NULL       |
+----+-------------------+------+-------------+--------+--------+------------+
9 rows in set (0.00 sec)

mysql> select * from department;
+----+------------+
| id | name       |
+----+------------+
|  1 | sales      |
|  2 | marketing  |
|  3 | HR         |
|  4 | Accounting |
|  5 | IT         |
+----+------------+
5 rows in set (0.00 sec)

*** Question 1 ****

mysql> select * from employee where join_date between '2013-01-21' and '2018-01-31';
+----+------+------+-------------+--------+--------+------------+
| id | name | age  | performance | salary | dep_id | join_date  |
+----+------+------+-------------+--------+--------+------------+
|  7 | c    |   25 |           4 |  78750 |      3 | 2015-03-04 |
|  8 | d    |   29 |           5 | 135000 |      5 | 2016-05-14 |
| 10 | f    |   32 |        NULL |  50750 |   NULL | 2017-09-01 |
+----+------+------+-------------+--------+--------+------------+
3 rows in set (0.00 sec)


***Question 2 ***
There have one option avg salary means in employee table. Because salary paid for a employee after certain period of time such as monthly or weekly or quaterly or semi year or yearly
So, for salary there have separate table because salary will record here with the date and employee id that will retrive by join query.

mysql> select * from salary;
+----+--------+--------+-------+
| id | emp_id | amount | month |
+----+--------+--------+-------+
|  1 |      1 |  45000 | 01    |
|  2 |      6 |  40000 | 01    |
|  3 |      7 |  35000 | 01    |
|  4 |      8 |  42000 | 01    |
|  5 |      9 |  37000 | 01    |
|  6 |     10 |  50000 | 01    |
|  7 |     11 |  52000 | 01    |
|  8 |     11 |  47000 | 01    |
|  9 |     12 |  43000 | 01    |
| 10 |      7 |  36000 | 02    |
| 11 |      8 |  40000 | 02    |
| 12 |      9 |  38000 | 02    |
| 13 |     10 | 490000 | 02    |
| 14 |     11 |  53000 | 02    |
| 15 |     11 |  48000 | 02    |
| 16 |     12 |  44000 | 02    |
+----+--------+--------+-------+
16 rows in set (0.00 sec)

mysql> select employee.id, employee.name, avg(salary.amount) from employee join (department, salary) on (employee.dep_id = department.id and employee.id = salar
y.emp_id) group by salary.emp_id;
+----+------+--------------------+
| id | name | avg(salary.amount) |
+----+------+--------------------+
|  1 | a    |              45000 |
|  6 | b    |              40000 |
|  7 | c    |              35500 |
|  8 | d    |              41000 |
|  9 | e    |              37500 |
+----+------+--------------------+
5 rows in set (0.00 sec)

