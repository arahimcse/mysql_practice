The MySQL LIKE Operator
The LIKE operator is used in a WHERE clause to search for a specified pattern in a column.

There are two wildcards often used in conjunction with the LIKE operator:

The percent sign (%) represents zero, one, or multiple characters
The underscore sign (_) represents one, single character
The percent sign and the underscore can also be used in combinations!


LIKE Operator	Description
WHERE CustomerName LIKE 'a%'	Finds any values that start with "a"
WHERE CustomerName LIKE '%a'	Finds any values that end with "a"
WHERE CustomerName LIKE '%or%'	Finds any values that have "or" in any position
WHERE CustomerName LIKE '_r%'	Finds any values that have "r" in the second position
WHERE CustomerName LIKE 'a_%'	Finds any values that start with "a" and are at least 2 characters in length
WHERE CustomerName LIKE 'a__%'	Finds any values that start with "a" and are at least 3 characters in length
WHERE ContactName LIKE 'a%o'	Finds any values that start with "a" and ends with "o"


select * from employee;
+----+-------------------+------+-------------+--------+
| id | name              | age  | performance | salary |
+----+-------------------+------+-------------+--------+
|  1 | a                 |   32 |           1 |  50000 |
|  6 | b                 |   30 |           3 |  66950 |
|  7 | c                 |   25 |           4 |  78750 |
|  8 | d                 |   29 |           5 | 135000 |
|  9 | e                 |   36 |           3 |  87550 |
| 10 | f                 |   32 |        NULL |  50750 |
| 11 | abdur rahim       |   30 |           2 |  25000 |
| 12 | sumaia aktar      |   30 |           1 |  18881 |
| 13 | md. razwon ahmmed |    4 |           4 |      0 |
+----+-------------------+------+-------------+--------+
9 rows in set (0.00 sec)

1. start with a 'a%'
select name from employee where name like 'a%';
+-------------+
| name        |
+-------------+
| a           |
| abdur rahim |
+-------------+

2. end with d '%d'
mysql> select * from employee where name like '%d';.
+----+-------------------+------+-------------+--------+
| id | name              | age  | performance | salary |
+----+-------------------+------+-------------+--------+
|  8 | d                 |   29 |           5 | 135000 |
| 13 | md. razwon ahmmed |    4 |           4 |      0 |
+----+-------------------+------+-------------+--------+

3. find that have hi in any opsition 
mysql> select * from employee where name like '%hi%';
+----+-------------+------+-------------+--------+
| id | name        | age  | performance | salary |
+----+-------------+------+-------------+--------+
| 11 | abdur rahim |   30 |           2 |  25000 |
+----+-------------+------+-------------+--------+

4. 

