The MySQL UNION Operator
The UNION operator is used to combine the result-set of two or more SELECT statements.

Every SELECT statement within UNION must have the same number of columns
The columns must also have similar data types
The columns in every SELECT statement must also be in the same order


The UNION operator selects only distinct values by default. To allow duplicate values, use UNION ALL:

mysql> select id from merits union select performance from employee;
+------+
| id   |
+------+
|    1 |
|    2 |
|    3 |
|    4 |
|    5 |
|    6 |
| NULL |
+------+

union --- return only the matching and mismatching distinct recornd from the two or more tables


mysql> select id from merits union all  select performance from employee;
+------+
| id   |
+------+
|    1 |
|    2 |
|    3 |
|    4 |
|    5 |
|    6 |
| NULL |
|    1 |
|    1 |
|    2 |
|    3 |
|    3 |
|    4 |
|    4 |
|    5 |
+------+
15 rows in set (0.00 sec)

union all --- retrive all the record from both table 


****Another advance implementation of union/union all 
write a sql to find the mismatch record from two tables

-- table 1
mysql> select * from t1;
+----+---------+
| id | name    |
+----+---------+
|  1 | java    |
|  2 |         |
|  3 | ASP.NET |
|  4 | C++     |
+----+---------+
4 rows in set (0.00 sec)

-- table 2
mysql> select * from t2;
+----+---------+
| id | name    |
+----+---------+
|  1 | ASP.NET |
|  2 | C++     |
|  3 | ASP.NET |
|  4 | ASP.NET |
|  5 | ASP.NET |
|  6 | ASP     |
+----+---------+
6 rows in set (0.00 sec)

-- query for the mismatch record

mysql> select name from(
    -> select name from t1
    -> union all
    -> select name from t2
    -> ) tb1 group by name having count(*) =1;
+------+
| name |
+------+
|      |
| ASP  |
| java |
+------+
3 rows in set (0.00 sec)


[Note: union operation just join the table t1+t2 for union all and t1+t2 (distinct) for union operation means select only one time if there have exit multiple times of same entry]





