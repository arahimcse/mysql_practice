The MySQL ANY and ALL Operators
The ANY and ALL operators allow you to perform a comparison between a single column value and a range of other values.

The ANY Operator
The ANY operator:

returns a boolean value as a result
returns TRUE if ANY of the subquery values meet the condition
ANY means that the condition will be true if the operation is true for any of the values in the range.

mysql> select * from employee where performance = any ( select id from merits where id>=2);
+----+-------------------+------+-------------+--------+
| id | name              | age  | performance | salary |
+----+-------------------+------+-------------+--------+
|  6 | b                 |   30 |           3 |  66950 |
|  7 | c                 |   25 |           4 |  78750 |
|  8 | d                 |   29 |           5 | 135000 |
|  9 | e                 |   36 |           3 |  87550 |
| 11 | abdur rahim       |   30 |           2 |  25000 |
| 13 | md. razwon ahmmed |    4 |           4 |      0 |
+----+-------------------+------+-------------+--------+
6 rows in set (0.03 sec)

[here performance column contain values from merits table. so, it get only values that is greater than 2 or equal to]



The ALL operator:

returns a boolean value as a result
returns TRUE if ALL of the subquery values meet the condition
is used with SELECT, WHERE and HAVING statements
ALL means that the condition will be true only if the operation is true for all values in the range.

mysql> select all name from employee;
+-------------------+
| name              |
+-------------------+
| a                 |
| b                 |
| c                 |
| d                 |
| e                 |
| f                 |
| abdur rahim       |
| sumaia aktar      |
| md. razwon ahmmed |
+-------------------+
9 rows in set (0.00 sec)


mysql> select * from employee where performance = all ( select id from merits where id = 3);
+----+------+------+-------------+--------+
| id | name | age  | performance | salary |
+----+------+------+-------------+--------+
|  6 | b    |   30 |           3 |  66950 |
|  9 | e    |   36 |           3 |  87550 |
+----+------+------+-------------+--------+
2 rows in set (0.01 sec)
