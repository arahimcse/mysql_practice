<?php
namespace App;
use PDO;
class mydb{
	/*
	MySQL Database Connection info
	*/
	public $dbcon;
	private $bduser = "root";
	private $dbpass = "";
	
	/*
	user data 
	*/
	private $name;
	private $id;
	private $designation;
	private $dob;

	public function __construct()
	{
		try{
			$this->dbcon = new \PDO('mysql:host=localhost;dbname=practice', $this->bduser, $this->dbpass);
			$this->dbcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			echo 'Successfully connect the mysql server';
		} catch(PDOException $e) {
			echo "Connection failed: " . $e->getMessage();
		}
	}
	public function prepare($data = '')
	{
		if(array_key_exists('name', $data))
		{
			$this->name = $data['name'];
		}
		
		if(array_key_exists('idno', $data))
		{
			$this->id = $data['idno'];
		}
		
		if(array_key_exists('designation', $data))
		{
			$this->designation = $data['designation'];
		}
		
		if(array_key_exists('dob', $data))
		{
			$this->dob = $data['dob'];
		}
		
		return $this;
	}
	
	public function table()
	{
		try{
			$sql = "create table if not exists ap_2015(
			id int not null auto_increment primary key,
			name varchar(25) null,
			idno int null,
			designation varchar(25) null,
			dob date
			)";
			$stmt = $this->dbcon->prepare($sql) or die("MySQL Connection Error");
			$stmt->execute();
			
		} catch (PDOException $e) {
			echo "Connection failed: " . $e->getMessage();
		}
		
	}
	
	public function insert()
	{
		try{
			$this->table();
			$sql = "insert into ap_2015(name, idno, designation, dob) values(?, ?, ?, ?)";
			$stmt = $this->dbcon->prepare($sql);
			$stmt->execute([
			$this->name,
			$this->id,
			$this->designation,
			$this->dob
			]);
			header('location:create.php');
		} catch (PDOException $e)
		{
			echo "Connection failed: " . $e->getMessage();
		}
	}
}
	