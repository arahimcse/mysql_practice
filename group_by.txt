The MySQL GROUP BY Statement
The GROUP BY statement groups rows that have the same values into summary rows, like "find the number of customers in each country".

The GROUP BY statement is often used with aggregate functions (COUNT(), MAX(), MIN(), SUM(), AVG()) to group the result-set by one or more columns.

* product table
mysql> select * from product;
+----+------+---------+
| id | name | cust_id |
+----+------+---------+
|  1 | aa   |       1 |
|  2 | bb   |       2 |
|  3 | cc   |       3 |
|  4 | dd   |       4 |
+----+------+---------+
4 rows in set (0.00 sec)


** customer table
mysql> select * from customer;
+----+------+---------------+---------+
| id | name | address       | country |
+----+------+---------------+---------+
|  1 | a    | 58 distrilary | USA     |
|  2 | d    | 1 distrilary  | USA     |
|  3 | c    | 23 distrilary | USA     |
|  4 | r    | 23 distrilary | UK      |
+----+------+---------------+---------+
4 rows in set (0.00 sec)

** group by [how many customer have in a country]
mysql> select country, count(*) as Customers from product join customer on customer.id = product.cust_id group by country;
+---------+-----------+
| country | Customers |
+---------+-----------+
| UK      |         1 |
| USA     |         3 |
+---------+-----------+
2 rows in set (0.01 sec)
