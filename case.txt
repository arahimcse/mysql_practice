Mysql case operator work as like as if-else statement. 

structure
 case
	when condition1 then results1
	when conditon2 then results2
	when conditionN then resultsN
	else result
 end;

****example
mysql> select name,
    -> case
    -> when id<2 then "under two"
    -> when id>2 then "above two"
    -> else "no comment"
    -> end as comments from employee;
+-------------------+-----------+
| name              | comments  |
+-------------------+-----------+
| a                 | under two |
| b                 | above two |
| c                 | above two |
| d                 | above two |
| e                 | above two |
| f                 | above two |
| abdur rahim       | above two |
| sumaia aktar      | above two |
| md. razwon ahmmed | above two |
+-------------------+-----------+
9 rows in set (0.00 sec)

