There is two table student(id, name), grade(id, subject, mark)
1. Show the average marks of each student ( There was a output table ) like output table.
2. Find the name of all student who have go marks>59

*** Solution ***
mysql> create table student(
    -> id int not null auto_increment primary key,
    -> name varchar(50) null
    -> );
Query OK, 0 rows affected (0.28 sec)

mysql> create table grade(
    -> id int not null auto_increment primary key,
    -> subject varchar(50) null,
    -> mark int not null,
    -> st_id int null,
    -> foreign key (st_id) references student (id)
    -> );
Query OK, 0 rows affected (0.28 sec)

****Data of student table
mysql> select * from student;
+----+--------------------+
| id | name               |
+----+--------------------+
|  1 | Abdur Rahim        |
|  2 | Sumaia Aktar       |
|  3 | Md. Razwon Ahammed |
|  4 | Tarek              |
+----+--------------------+
4 rows in set (0.00 sec)

****Data of grade table
mysql> select * from grade;
+----+-------------------+------+-------+
| id | subject           | mark | st_id |
+----+-------------------+------+-------+
|  1 | Bangla            |   62 |     1 |
|  2 | English           |   52 |     1 |
|  3 | Math              |   83 |     1 |
|  4 | Physics           |   84 |     1 |
|  5 | Chemistry         |   72 |     1 |
|  6 | Biology           |   62 |     1 |
|  7 | Higher Math       |   76 |     1 |
|  8 | Bangla            |   77 |     2 |
|  9 | English           |   62 |     2 |
| 10 | Math              |   40 |     2 |
| 11 | History           |   53 |     2 |
| 12 | Political Science |   63 |     2 |
| 13 | Economic          |   60 |     2 |
| 14 | Bangla            |   61 |     3 |
| 15 | English           |   51 |     3 |
| 16 | Economic          |   45 |     3 |
| 17 | History           |   47 |     3 |
+----+-------------------+------+-------+
17 rows in set (0.00 sec)

**** Question 01 ****
mysql> select student.name, avg(grade.mark) as 'Average Marks' from student join
 grade on student.id = grade.st_id group by grade.st_id;
+--------------------+---------------+
| name               | Average Marks |
+--------------------+---------------+
| Abdur Rahim        |       70.1429 |
| Sumaia Aktar       |       59.1667 |
| Md. Razwon Ahammed |       51.0000 |
+--------------------+---------------+
3 rows in set (0.03 sec)

**** Question 02 ****
mysql> select student.name from student join grade on student.id = grade.st_id g
roup by grade.st_id having avg(grade.mark)>59;
+--------------+
| name         |
+--------------+
| Abdur Rahim  |
| Sumaia Aktar |
+--------------+
2 rows in set (0.00 sec)


